{
  description = "AltServer for AltStore, but on-device";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
  };

  outputs = { self, ... } @ inputs:
    let
      system = "x86_64-linux";
      pname = "altserver-linux";
      version = "0.0.5";

      pkgs = import inputs.nixpkgs { inherit system; };
      stdenv = pkgs.clangStdenv;
    in
      {
        packages."${system}" = {
          corecrypto = stdenv.mkDerivation {
            pname = "corecrypto";
            version = "2022";

            src = pkgs.requireFile {
              name = "corecrypto.zip";
              url = "https://developer.apple.com/security/";
              sha256 = "11srh2d902f8l8xd8mrpzpann5znn5q3ksmn3nmc1ws0w4h8wngb";
            };

            patches = [ ./corecrypto.patch ];

            unpackPhase = ''
              runHook preUnpack
              unzip $src
              mv corecrypto/* .
              rmdir corecrypto
              runHook postUnpack
            '';

            postConfigure = ''
              sed -i '/all: CMakeFiles\/corecrypto_test/d' CMakeFiles/Makefile2
              sed -i '/all: CMakeFiles\/corecrypto_perf/d' CMakeFiles/Makefile2
            '';

            nativeBuildInputs = with pkgs; [ cmake pkg-config unzip python3 ];
          };

          cpprestsdk = stdenv.mkDerivation {
            pname = "cpprestsdk";
            version = "2.10.18";

            src = pkgs.fetchFromGitHub {
              owner = "microsoft";
              repo = "cpprestsdk";
              rev = "v2.10.18";
              # deepClone = true;
              # fetchSubmodules = true;
              sha256 = "sha256-RCt6BIFxRDTGiIjo5jhIxBeCOQsttWViQcib7M0wZ5Y=";
            };

            buildInputs = with pkgs; [ openssl zlib boost websocketpp ];
            nativeBuildInputs = with pkgs; [ cmake pkg-config ];
          };

          altserver-linux = (
              stdenv.mkDerivation {
                inherit pname version;

                src = pkgs.fetchFromGitHub {
                  owner = "NyaMisty";
                  repo = "AltServer-Linux";
                  rev = "4a2a9e8425c43fa5d20e5f95d4bd758f21fd5cfc";
                  fetchSubmodules = true;
                  sha256 = "sha256-fkbVlRLYPJhIxRuw3a0iGhk8lb/cthKYFO6hFMJbfKc=";
                };

                patches = [ ./altserver.patch ];

                prePatch =
                  ''
                  # Convert file line endings so the patch applies
                  dos2unix upstream_repo/AltServer/ConnectionManager.cpp
                  dos2unix upstream_repo/AltSign/Archiver.cpp
                  ''
                  + pkgs.lib.optionalString (!stdenv.hostPlatform.isMusl) ''
                  rm shims/muslfix.cpp
                  '';

                #preBuild = ''
                #  NIX_CFLAGS_COMPILE="-I${self.packages."${system}".cpprestsdk}/include $NIX_CFLAGS_COMPILE"
                #'';

                installPhase = ''
                  runHook preInstall

                  mkdir -p $out/bin
                  cp AltServer $out/bin/altserver

                  runHook postInstall
                '';

                buildInputs = [
                  pkgs.avahi-compat
                  pkgs.boost
                  self.packages."${system}".corecrypto
                  self.packages."${system}".cpprestsdk
                  pkgs.libuuid
                  pkgs.libzip
                  pkgs.openssl
                  pkgs.zlib
                ];

                nativeBuildInputs = with pkgs; [ python3 dos2unix pkg-config ];
              }
          );
        };
      };
}
